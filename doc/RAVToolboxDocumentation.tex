\documentclass[12pt,A4,english]{article}
\usepackage[
        a4paper,% other options: a3paper, a5paper, etc
        %left=1cm,
        %right=2cm,
        %top=3cm,
        %bottom=4cm,
        vmargin=2cm, % to make vertical margins equal to 2cm.
        hmargin=2cm, % to make horizontal margins equal to 3cm.
        % use margin=3cm to make all margins  equal to 3cm.
]{geometry}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{soul}
\usepackage{graphicx}
\usepackage{subfig} % For subfigures

\newcommand{\Prob}{\mathbb{P}}
\newcommand{\Exp}{\mathbb{E}}
\newcommand{\MPol}{\mu}

\begin{document}
\title{Documentation for RAVToolbox}
\maketitle

\section{Introduction}
\label{sec:introduction}

This toolbox performs stochastic reachability analysis for dynamical systems.
The backward reachability analysis will be based on dynamic
programming~\cite{summers_verification_2010}. The forward reachability analysis
will be done by considering the ``time-inverted'' dynamics. For uncontrolled
linear systems, we incorporate Fourier analysis to perform highly efficient
reachability analysis~\cite{AbrahamArxiv2017}.  We note that the deterministic
reachability can be achieved via Level set toolbox.

In backward reachability analysis, two distinct reach-avoid problems in finite
horizon are considered --- 1)  the terminal hitting time, and 2) the first
hitting time case. Each of these cases are characterized by a target set and a
safe set.

\subsection{Goals}

\begin{enumerate}
    \item Python implementation of stochastic reachability analysis
    \item A codebase that is extendable to higher dimensions
    \item Implement parallel processing
\end{enumerate}

% subsection Goals (end)

\subsection{Milestones of the project (To be achieved)}

\begin{enumerate}
    \item \st{Double integrator stochastic reach-avoid (terminal-time hitting)}
    \item Double integrator reach, avoid and viable set computation
    \item Generalized stochastic reach-avoid (terminal-time hitting)
    \item Generalized reach, avoid and viable set computation
    \item Double integrator stochastic forward reachability analysis (inverted
        time)
    \item Generalized stochastic forward reachability analysis (inverted
        time)
    \item Double integrator stochastic reach-avoid (first-time hitting)
    \item Generalized stochastic reach-avoid (first-time hitting)
\end{enumerate}

% subsection Milestones (end)

% section introduction (end)

\section{Backward reach-avoid  --- Terminal time hitting}

Consider the discrete-time time-invariant system,
\begin{align}
    x[t+1]&=f(x[t],u[t],d[t])\label{eq:sys}
\end{align}
with state $x[t]\in \mathcal{X}\subseteq \mathbb{R}^n$, input $u[t]\in
\mathcal{U} \subseteq \mathbb{R}^m$, disturbance $d[t]\in \mathcal{D} \subseteq
\mathbb{R}^p$, and system dynamics $f: \mathcal{X}\times \mathcal{U} \times
\mathcal{D} \rightarrow \mathcal{X}$. Let the time horizon be $T$. 

Assume $ \mathcal{U}$ be a compact sets (Why?) and $f$ is a
Borel-measurable function.  We assume the disturbance $d[t]$ is a independent
and identically distributed discrete random process. For any $t\in[0,T-1]$, $d[t]$
is a random variable defined in $( \mathcal{D}, \sigma( \mathcal{D}), \Prob_d)$.
Here, $\sigma( \mathcal{D})$ denotes the sigma-algebra associated with countable
set $ \mathcal{D}$.  

Let $ \mathcal{M}$ denote all the Markov policies
$\MPol=\{\mu_0(\cdot),\mu_1(\cdot),\ldots,\mu_{T-1}(\cdot)\}$ where $\mu_t:
\mathcal{X} \rightarrow \mathcal{U}$ are Borel-measurable maps providing a
control action for a given state. From \eqref{eq:sys}, we see that the
trajectory of the system $\xi(t;x_0,\MPol)=x[t]$ is a random process for a
given policy $\MPol$ and initial state $x[0]=x_0\in \mathcal{X}$. For each
$t\in[0,T-1]$, $x[t]$ is a random variable defined in $( \mathcal{X},
\mathcal{B}( \mathcal{X}), \Prob_{x}^{t,\MPol})$ conditioned on $x[t-1]$. Here, $
\mathcal{B}(\cdot)$ denotes the Borel algebra, and the conditional probability
measure $\Prob_{x}^{t,\MPol}$ is induced from the probability measure $\Prob_d$
and the borel measurable function $f$. We denote the joint probability measure
governing the stochastics of $\xi(\cdot;\MPol,x_0)$ as $\Prob_{\xi}^{\MPol,x_0}$.

We define the target set as $ \mathcal{T}\subseteq \mathcal{X}$ and the safe set
as $\mathcal{K}\subseteq \mathcal{X}$. In backward reach-avoid analysis, we
compute the probability of the system \eqref{eq:sys} initialized at $x_0\in
\mathcal{X}$ with control policy $\mu\in \mathcal{M}$ reaches $ \mathcal{T}$
while avoiding $ \mathcal{X}\setminus \mathcal{K}$. This probability is given by
\begin{align}
    \hat{r}_{x_0}^\MPol( \mathcal{T}, \mathcal{K})&=
    \Prob_{\xi}^{\MPol,x_0}\big\{\{\xi(T;\MPol,x_0)\in \mathcal{T}\} \wedge \{\forall
t\in[0,T-1], \xi(t;\MPol,x_0)\in
\mathcal{K}\}\big\}\label{eq:reachavoidProbTerminal}\\
&= \Prob_{\xi}^{\MPol,x_0}\big\{\{x[T]\in \mathcal{T}\} \wedge \{\forall
t\in[0,T-1], x[t]\in \mathcal{K}\}\big\} \nonumber
\end{align}
Note that the target set $ \mathcal{T}$ need not be a subset of the safe set $
\mathcal{K}$.  We observe that
\begin{align}
    \left(\prod_{t=0}^{T-1} 1_{ \mathcal{K}}(x[t])\right)1_{
    \mathcal{T}}(x[T])&= \begin{cases}
        \begin{array}{ll}
            1 & \mbox{ if } x[T]\in \mathcal{T} \wedge \forall
t\in[0,T-1], x[t]\in \mathcal{K} \\
0 & \mbox{ otherwise}
        \end{array}
    \end{cases}\label{eq:prod_indc_RAT}
\end{align}
where for any $S\subseteq \mathcal{X}$, $1_S(y)=1$ if $y\in S$ and zero
otherwise. Using \eqref{eq:prod_indc_RAT}, we have
\begin{align}
    \hat{r}_{x_0}^\MPol( \mathcal{T}, \mathcal{K})&=
    \Exp_{\xi}^{\MPol,x_0}\left[\left(\prod_{t=0}^{T-1} 1_{ \mathcal{K}}(x[t])\right)1_{
    \mathcal{T}}(x[T])\right]\label{eq:exp_RAT}
\end{align}
As discussed in~\cite{summers_verification_2010}, we can evaluate
\eqref{eq:exp_RAT} for a given policy $\MPol$ and initial state $x_0$ by a
recursion on functions $V_k^{\MPol,x_0}: \mathcal{X} \rightarrow [0,1]$:
\begin{subequations}
    \begin{align}
        V_T^{\MPol}(y)&=1_{ \mathcal{T}}(y)\\
        V_t^{\MPol}(y)&=1_{ \mathcal{K}}(y) \int_{ \mathcal{X}}
        V_{t+1}^{\MPol,x_0}(y^+) Q^\MPol_t(y^+,y,u)dy^+
    \end{align}
\end{subequations}
for $t\in\{0,\ldots,T-1\}$ where
$Q^\MPol_t(y^+,y,u)=\Prob_{x}^{t,\MPol}\{x[t+1]=y^+\vert x[t]=y\}$ and
$\mu_t(y)=u$. Clearly, $$V_0^{\MPol}(y)=\hat{r}_{y}^\MPol( \mathcal{T},
\mathcal{K}).$$ Since $d[\cdot]$ is assumed to
be an i.i.d process,
$Q_t^\MPol(\cdot)$ is time-invariant,
\begin{align}
    Q^\MPol_t(y^+,y,u)&=\Prob_d\{d \in \mathcal{D} \vert
y^+=f(y,u,d)\}.\label{eq:Qdef}
\end{align}

From~\cite[Definition 10]{summers_verification_2010}, a Markov policy
$\MPol^\ast$ is a maximal reach-avoid policy in the terminal sense if and only if
$\hat{r}^{\MPol^\ast}_{x_0}(\mathcal{T} , \mathcal{K} ) = \sup_{\MPol\in
\mathcal{M}}\hat{r}^{\MPol}_{x_0}( \mathcal{T} , \mathcal{K})$, for all $x_0\in
X$. From~\cite[Theorem 11]{summers_verification_2010}, we have a dynamic
programming recursion to compute the control policy $\MPol^\ast$ that guarantees
maximum probability of reaching $ \mathcal{T}$ while avoiding the unsafe set $
\mathcal{X}\setminus \mathcal{K}$. The dynamic programming recursion for
computing the maximal reach-avoid probability is 
\begin{subequations}
    \begin{align}
        W_T^{\MPol}(y)&=1_{ \mathcal{T}}(y)\\
        W_t^{\MPol}(y)&=\sup_{u\in \mathcal{U}}1_{ \mathcal{K}}(y) \int_{ \mathcal{X}}
        W_{t+1}^{\MPol}(y^+) Q^\MPol_t(y^+,y,u)dy^+ \\
        \mu^\ast_t(y)&=\arg\sup_{u\in \mathcal{U}}1_{ \mathcal{K}}(y) \int_{ \mathcal{X}}
        W_{t+1}^{\MPol}(y^+) Q^\MPol_t(y^+,y,u)dy^+
    \end{align}\label{eq:maxm}
\end{subequations}
for $t\in\{0,\ldots,T-1\}$ and
$W_0^{\MPol}(y)=\sup_{\MPol\in \mathcal{M}}\hat{r}^{\MPol^\ast}_{y}(\mathcal{T} , \mathcal{K} )$.

The dynamic programming recursion for computing the minimal reach-avoid probability is 
\begin{subequations}
    \begin{align}
        Z_T^{\MPol}(y)&=1_{ \mathcal{T}}(y)\\
        Z_t^{\MPol}(y)&=\inf_{u\in \mathcal{U}}1_{ \mathcal{K}}(y) \int_{ \mathcal{X}}
        Z_{t+1}^{\MPol}(y^+) Q^\MPol_t(y^+,y,u)dy^+ \\
        \mu^\ast_t(y)&=\arg\inf_{u\in \mathcal{U}}1_{ \mathcal{K}}(y) \int_{ \mathcal{X}}
        Z_{t+1}^{\MPol}(y^+) Q^\MPol_t(y^+,y,u)dy^+
    \end{align}\label{eq:minm}
\end{subequations}
for $t\in\{0,\ldots,T-1\}$ and $Z_0^{\MPol}(y)=\inf_{\MPol\in \mathcal{M}}\hat{r}^{\MPol^\ast}_{y}(\mathcal{T} , \mathcal{K} )$.

\subsection{Reach set computation}

We set $ \mathcal{T}\subseteq \mathcal{X}$ as the desired target, and set $
\mathcal{K}$ as the entire state space $ \mathcal{X}$. The reach computation is
maximal in the first sense~\cite[Theorem 11]{summers_verification_2010}. The
recursion to be used is \eqref{eq:maxm}.

\subsection{Viable set computation}

We set $ \mathcal{T}$ as the entire state space $ \mathcal{X}$ (since it is
irrelevant), and set $ \mathcal{K}\subseteq \mathcal{X}$ as the desired safe set.
The viability computation is maximal in the first sense~\cite[Theorem
11]{summers_verification_2010}.  The
recursion to be used is \eqref{eq:maxm}.

\subsection{Avoid set computation --- Yet to be implemented}

Let the set to be avoided be $ \mathcal{O}\subseteq \mathcal{X}$. The
computation is minimal in the first sense~\cite[Like Theorem
8]{summers_verification_2010}. The recursion to be used is \eqref{eq:minm} with
$ \mathcal{T}= \mathcal{O}$ and $ \mathcal{K}= \mathcal{X}$.

\subsection{Demonstration}

We consider the double integrator dynamics. Compared to the general nonlinear
system \eqref{eq:sys}, the dynamics of a double integrator is LTI,
\begin{align}
    x[k+1]&=Ax[k]+Bu[k]+Fd[k]\label{eq:sys_di}
\end{align}
with state $x[k]\in \mathcal{X}=[-1,1]\times[-1,1]$, input $u[k]\in
\mathcal{U}=[-0.5,0.5]$, disturbance $d[k]\in \mathcal{D}=\{-0.5,0,0.5\}$,
$\Prob\{d[k]=-0.5\}=\Prob\{d[k]=0.5\}=0.3$, $\Prob\{d[k]=0\}=0.4$. With sampling
time $T_s=0.05$, the matrices are
\begin{align}
    A= \left[ {\begin{array}{cc} 1  &  T_s \\ 0  & 1  \end{array} } \right],\ 
    B= \left[ {\begin{array}{c} \frac{T_s^2}{2} \\ T_s  \end{array} } \right],\mbox{ and }
    F= \left[ {\begin{array}{c} \frac{T_s^2}{2} \\ T_s  \end{array} } \right].
\end{align}

The gridding of $ \mathcal{X}$ was done in increments of $0.005$ and of $
\mathcal{U}$ in increments of $0.5$. The code for this is available as
\texttt{testing\_RAT.py}. The computations were done on an Intel i7 CPU running
at 2.1 GHz and 8GB RAM.

\subsubsection{Viability analysis}

\begin{figure}
\centering
\newcommand{\figw}{0.4}
\subfloat[]{\includegraphics[width=\figw\linewidth]{imgs/viab.png}}
\qquad
\subfloat[]{\includegraphics[width=\figw\linewidth]{imgs/viab_3D.png}}
    \caption{Viability computation for the dynamics in \eqref{eq:sys_di}
    considered for a time horizon of $10$ time steps ($10T_s=0.5$ seconds). The
total runtime was found to be $310$ seconds.}\label{fig:viab}
\end{figure}

The target set $ \mathcal{T}=\mathcal{X}$, and the safe set $
\mathcal{K}=[-0.75,0.75]\times[-0.75,0.75]$. Figure~\ref{fig:viab} shows the
results. The computational time was $310$ seconds.

\subsubsection{Reachability analysis}

\begin{figure}
\centering
\newcommand{\figw}{0.4}
\subfloat[]{\includegraphics[width=\figw\linewidth]{imgs/reach.png}}
\qquad
\subfloat[]{\includegraphics[width=\figw\linewidth]{imgs/reach_3D.png}}
    \caption{Reachability computation for the dynamics in \eqref{eq:sys_di}
    considered for a time horizon of $10$ time steps ($10T_s=0.5$ seconds). The
total runtime was found to be $603$ seconds.}\label{fig:reach}
\end{figure}

The target set is a circle of radius $0.25$ centered at origin, and the safe set
$ \mathcal{K}= \mathcal{X}$. Figure~\ref{fig:reach} shows the results. The
computational time was $603$ seconds.
% subsubsection reachability analysis (end)
% subsection System dynamics (end)

\section{Backward Reach-Avoid --- First time hitting}

\section{Forward reachability analysis}

\begin{enumerate}
    \item Dynamic programming based formulation with dynamics inverted-time 
    \item Abraham's work on Fourier transform based forward reachability for
        uncontrolled systems.
\end{enumerate}

\section{Class definition and properties}
\label{sec:class_definition_and_properties}

\begin{enumerate}
    \item Grid class
        \begin{enumerate}
            \item A way to describe the target $ \mathcal{T}$, safe set $
                \mathcal{K}$ and the state space $ \mathcal{X}$
            \item Properties:
                \begin{enumerate}
                    \item $n$ Dimension (should be secure)
                    \item Mesh is $N\times n$ numpy array for $N$ grid points
                    \item Mesh is iterable
                \end{enumerate}
            \item Methods
                \begin{enumerate}
                    \item Grid to Square for plotting
                \end{enumerate}
            \item Constructor
                \begin{enumerate}
                    \item Takes in limits and step size
                \end{enumerate}
            \item A method to link the iteration (probability) to a grid point
            \item Objects defined in the problem scripts:
                \begin{enumerate}
                    \item Input mesh $ \mathcal{U}$
                    \item Disturbance mesh  $ \mathcal{D}$
                    \item Target $ \mathcal{T}$
                    \item Safe set $ \mathcal{K}$
                    \item State space $ \mathcal{K}$
                \end{enumerate}
        \end{enumerate}
    \item Disturbance class
        \begin{enumerate}
            \item discrete disturbance
            \item[[Later]] continuous
            \item a vector of probability mass function
            \item a vector of realizations
            \item Methods:
                \begin{enumerate}
                    \item Plot pmf as stem (1D)
                    \item[[Later]] Plot pmf as surf (2D)
                \end{enumerate}
        \end{enumerate}
    \item System class
        \begin{enumerate}
            \item Variables:
                \begin{enumerate}
                    \item Continuous/discrete system flag (we will be working on
                        discrete system for now)
                    \item Handle for the time-invariant (possibly nonlinear) system dynamics - $f(x,u,w)$, 
                    \item[[Later]] Continuous time case, 
            \end{enumerate}
            \item Method:
                \begin{enumerate}
                    \item Populate xplus using $f$
                \end{enumerate}
        \end{enumerate}
    \item Problem script
\end{enumerate}
% section class_definition_and_properties (end)

%\section{Open Issues}
%\label{sec:open_issues}
%
%
%
%% section open_issues (end)

\bibliographystyle{IEEEtran}
\bibliography{RAVTbx.bib}
\end{document}
