# RAV Toolbox --- ReachAvoidViability Toolbox#

For systems perturbed by a disturbance, this toolbox provides probabilistic guarantees on task completion and safety. This is achieved by performing the backward stochastic reach-avoid and viability analysis as described in [Summers, Automatica 2011](http://www.sciencedirect.com/science/article/pii/S0005109810003547).

The documentation can be found [here](https://bitbucket.org/abyvinod/ravtoolbox/raw/master/doc/RAVToolboxDocumentation.pdf). The following plots show the stochastic viability of a double integrator created using the RAV toolbox.

![Oops](https://bitbucket.org/abyvinod/ravtoolbox/raw/master/doc/imgs/viab.png "Viability contour plots for a double integrator")
![Oops](https://bitbucket.org/abyvinod/ravtoolbox/raw/master/doc/imgs/viab_3D.png "Viability contour plots for a double integrator")

### How do I get set up? ###

* The MATLAB version of the codes have been tested in MATLAB 2015b. The
  additional requirements are: (currently none).
* The Python code is built for Python 3.5. While our code base should work for any setup of Python 3.5, we have provided our conda environment in order to  ensure no conflict of dependencies.

## Using conda

You can rebuild our exact environment using the conda environment setup file - ravtoolbox.yml. Here are the steps to follow to replicate our python environment:

1. Install [conda](http://conda.pydata.org/docs/install/quick.html)
1. Open your terminal window and cd to the setup folder inside our toolbox.
1. type 'conda update conda' to get the latest conda setup
1. type 'conda env create -f ravtoolbox.yml -n AnyEnvironmentNameYouLike'
1. Run testing.py to ensure everything is working. 

### Contribution guidelines ###

* We will be following the [Python Style Guide](https://www.python.org/dev/peps/pep-0008/) for writing up the code.
* Kindly update the documentation with the reasoning for any changes.

### Who do I talk to? ###

* Abraham (aby[dot]vinod[at]gmail[dot]com)
* Joseph (gleasonj[at]unm[dot]edu)