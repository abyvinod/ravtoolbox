classdef GridSpace
    properties
        grid_pts
        dimension        
    end
    methods
        function obj = GridSpace(grid_pts)
            obj.grid_pts = grid_pts;
            obj.dimension= size(grid_pts,2);
        end
    end
    methods (Static)
        obj = init_with_vectors(varargin);
    end
end