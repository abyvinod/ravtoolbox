function obj = init_with_vectors(varargin)
%
% function obj = init_with_vectors(varargin)
%
%   Description:
%   ============
%   Instantiate a GridSpace object with an arbitrary number or vectors. The
%   code will generate a rectangular grid space from the vectors that are
%   given, i.e. for some fixed [x_2,...,x_n] x_1 can be any point in the
%   provided vector varargin{1}.
%   
%   Code can, with reasonable efficiency, create grid spaces for about 1e6
%   grid points. The number of grid points created is equal to the product
%   of the lenghts of the given input vectors.
%
%   Inputs:
%   =======
%   varargin - Variable length input argument. All arguments should be
%   vectors of either (1 x n) or (n x 1) size.
%
%   Outputs:
%   ========
%   obj - Instance of GridSpace object.
%
%   Example:
%   ========
%   x = linspace(-1,1,10);
%   gs = GridSpace.init_with_vectors(x,x,x);
%

% Determine number of vectors and find lengths
no_of_vecs = length(varargin);
% initialize vector length array and fill, may be able to eliminate this in
% future version
len_vec = zeros(1,no_of_vecs);
for i = 1:no_of_vecs
    len_vec(i) = length(varargin{i});
end

% compute grid points
k = 1;
now_index = ones(1,no_of_vecs);
grid_pt = zeros(1,no_of_vecs);
grid_pts = zeros(prod(len_vec),no_of_vecs);
while(now_index(1) <= len_vec(1))
    % find gridpt and set into gridpts
    for i = 1:no_of_vecs
        grid_pt(i) = varargin{i}(now_index(i));
    end
    grid_pts(k,:) = grid_pt;
    k = k + 1;
    
    % increment index array
    now_index(no_of_vecs) = now_index(no_of_vecs) + 1;
    for i = no_of_vecs:-1:2
        if(now_index(i) > len_vec(i))
            now_index(i-1) = now_index(i-1) + 1;
            now_index(i) = 1;
        end
    end
end

% instantiate object
obj = GridSpace(grid_pts);

end