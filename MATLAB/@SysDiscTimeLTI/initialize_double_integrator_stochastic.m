function obj=initialize_double_integrator_stochastic()
    T=0.1;
    xmin=-1;
    xmax=1;
    xinc=0.01;
    umin=0;
    uinc=0.1;
    umax=.5;
    wmin=-0.3;
    wmax=0.3;
    winc=0.3;
    
    prob=[0.3,0.4,0.3];
    x=xmin:xinc:xmax;
    u=umin:uinc:umax;
    w=wmin:winc:wmax;
    A=[1,T;
       0,1];
    B=[T^2/2;
        T];
    G=[0;1];
    X=GridSpace.init_with_vectors(x,x);
    U=GridSpace.init_with_vectors(u);
    W=GridSpace.init_with_vectors(w);
    obj=SysDiscTimeLTI(A,B,X,U,G,W,prob);
end