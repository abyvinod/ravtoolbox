function obj=initialize_double_integrator_deterministic()
    T=0.1;
    xmin=-1;
    xmax=1;
    xinc=0.1;
    umin=0;
    uinc=0.1;
    umax=.5;
    
    x=xmin:xinc:xmax;
    u=umin:uinc:umax;
    A=[0,1;
       0,0];
    B=[T^2/2;
        T];
    X=GridSpace.init_with_vectors(x,x);
    U=GridSpace.init_with_vectors(u);
    obj=SysDiscTimeLTI(A,B,X,U);
end