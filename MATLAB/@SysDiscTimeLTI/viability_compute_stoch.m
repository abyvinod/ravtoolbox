function Vt=viability_compute_stoch(SysDiscTimeLTI,no_of_steps)
    fprintf('Viability computation for %d steps\n',no_of_steps);
    
    % TODO (Abraham) : Check if safe_set is actually a subset of the
    % state-space
    
    % Convert safe_set.grid_pts into indexes
    
    len_state=length(SysDiscTimeLTI.X.grid_pts);
    len_disturb=length(SysDiscTimeLTI.disturb_space.grid_pts);
    len_input=length(SysDiscTimeLTI.U.grid_pts);
    
    Vt=zeros(len_state,no_of_steps+1);
    Vt(:,no_of_steps+1)=1;
    
    t=no_of_steps;
    while t>=1
        for i=1:len_state
            xplus_temp=reshape(SysDiscTimeLTI.xplus{i},len_disturb*len_input,[]);
            Vt_temp=interp(1:len_state,Vt(xplus_temp,t+1);
            
            expectation_Vtplus1=SysDiscTimeLTI.disturb_prob*reshape(,len_disturb,len_input);
            
            Vt(i,t)=max(expectation_Vtplus1);
        end
        t=t-1;
    end
end