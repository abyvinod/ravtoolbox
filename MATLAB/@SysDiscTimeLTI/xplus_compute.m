function xplus=xplus_compute(SysDiscTimeLTI)
    xplus=cell(length(SysDiscTimeLTI.X.grid_pts),1);
    if SysDiscTimeLTI.has_stochastic_dynamics
        input=SysDiscTimeLTI.U.grid_pts;
        state=SysDiscTimeLTI.X.grid_pts;
        len_input=length(input);
        len_state=length(state);
        % TODO: Assumes symmetric dimensions
        len_state_1D=sqrt(len_state);
        disturb=SysDiscTimeLTI.disturb_space.grid_pts;
        len_disturb=length(disturb);
        
        state_zero=state(1,:);
        state_zero_MAT=repmat(state_zero',len_disturb,len_input);
        state_inc=abs(state(2,2)-state(1,1));
        % TODO: Assumes equal increment in all dimensions
%         state_inc_MAT=repmat(state_inc',len_disturb,len_input);
        
        for x_indx=1:len_state
            x_curr_times_A=SysDiscTimeLTI.A*state(x_indx,:)';
            u_curr_times_B=SysDiscTimeLTI.B*input';
            w_curr_times_disturb_matrix=reshape(SysDiscTimeLTI.disturb_matrix*disturb',[],1);
            
            % Columnwise states are repeated across rows and columns
            x_curr_times_A_MAT=repmat(x_curr_times_A,len_disturb,len_input);
            % Columns have constant input
            u_curr_times_B_MAT=repmat(u_curr_times_B,len_disturb,1);
            % Rows have constant disturbance
            w_curr_times_disturb_matrix_MAT=repmat(w_curr_times_disturb_matrix,1,len_input);
            
            % Dynamics propagation
            xplus_MAT=x_curr_times_A_MAT+u_curr_times_B_MAT+w_curr_times_disturb_matrix_MAT;
            % Index generation
            xplus_indx_MAT=round((xplus_MAT-state_zero_MAT)./state_inc)+1;
            
            xplus_indx_MAT(xplus_indx_MAT<1)=NaN;
            xplus_indx_MAT(xplus_indx_MAT>len_state_1D)=NaN;
            xplus_state_indx_MAT=zeros(len_disturb,len_input);
            i=1;
            while i<=len_disturb
                % Index compacting to match with the gridding
                xplus_state_indx_MAT(i,:)=len_state_1D*(xplus_indx_MAT(2*i-1,:)-ones(1,len_input))+xplus_indx_MAT(2*i,:);
                i=i+1;
            end
            xplus{x_indx}=xplus_state_indx_MAT;
        end
    else
        disp('Skipping xplus computation for deterministic systems');
    end
end