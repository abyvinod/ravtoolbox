classdef SysDiscTimeLTI
    properties
        X
        U
        A
        B
        disturb_matrix
        disturb_space
        disturb_prob
        xplus
        has_stochastic_dynamics=0;
    end
    methods
        Vt=viability_compute_det(SysDiscTimeLTI,no_of_steps);
        Vt=viability_compute_stoch(SysDiscTimeLTI,no_of_steps);
        Vt=reach_avoid_compute_det(SysDiscTimeLTI,target_set,no_of_steps);
        Vt=reach_avoid_compute_stoch(SysDiscTimeLTI,target_set,no_of_steps);
        Vt=reach_compute_det(SysDiscTimeLTI,target_set,no_of_steps);
        Vt=reach_compute_stoch(SysDiscTimeLTI,target_set,no_of_steps);
        xplus=xplus_compute(SysDiscTimeLTI);
        function obj = SysDiscTimeLTI(A,B,X,U,dist_matrix,dist_space,prob_dist)
            if nargin==4
                obj.A = A;
                obj.B = B;
                obj.U=U;
                obj.X=X;
                obj.xplus=obj.xplus_compute();
            elseif nargin==7
                obj.A = A;
                obj.B = B;
                obj.U=U;
                obj.X=X;
                obj.has_stochastic_dynamics=1;
                obj.disturb_matrix=dist_matrix;
                obj.disturb_space=dist_space;
                obj.disturb_prob=prob_dist;                
                obj.xplus=obj.xplus_compute();                
            else
                error('Specify either a deterministic system (4 arguments) or a stochastic system (7 arguments)');
            end            
        end
        
        function Vt=viability_compute(SysDiscTimeLTI,no_of_steps)
            if SysDiscTimeLTI.has_stochastic_dynamics==1
                Vt=SysDiscTimeLTI.viability_compute_stoch(no_of_steps);
            else
                Vt=SysDiscTimeLTI.viability_compute_det(no_of_steps);
            end
        end
        
        function Vt=reach_avoid_compute(SysDiscTimeLTI,target_set,no_of_steps)
            if SysDiscTimeLTI.has_stochastic_dynamics==1
                Vt=SysDiscTimeLTI.reach_avoid_compute_stoch(target_set,no_of_steps);
            else
                Vt=SysDiscTimeLTI.reach_avoid_compute_det(target_set,no_of_steps);
            end
        end
        
        function Vt=reach_compute(SysDiscTimeLTI,target_set,no_of_steps)
            if SysDiscTimeLTI.has_stochastic_dynamics==1
                Vt=SysDiscTimeLTI.reach_compute_stoch(target_set,no_of_steps);
            else
                Vt=SysDiscTimeLTI.reach_compute_det(target_set,no_of_steps);
            end
        end
    end    
    
    methods (Static)
        obj = initialize_double_integrator_deterministic();
        obj = initialize_double_integrator_stochastic();
    end
end