from setuptools import setup

setup(
    name='ravtoolbox',
    version='0.1',
    description='Toolbox for computation of stochastic reach avoid sets',
    url='https://bitbucket.org/abyvinod/ravtoolbox',
    author='Abraham Vinod; Joseph Gleason',
    author_email='abyvinod@unm.edu; gleasonj@unm.edu',
    license='MIT',
    packages=['ravtoolbox'],
    install_requires=[
        'numpy',
        'matplotlib',
    ],
    zip_safe=False
)
