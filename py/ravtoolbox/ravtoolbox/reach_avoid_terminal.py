import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from Grid import Grid
from GriddedSet import GriddedSet
from SystemDynamics import SystemDynamics
from Disturbance import Disturbance

def reach_avoid_terminal(target_set,safe_set,dynamical_system,disturbance,time_horizon):
    """ Reach-Avoid terminal function useful to perform the maximal stochastic
    reachability analysis. This function can be used to perform backward
    reach-avoid, backward reach, and backward viability analysis.

    Parameters:
        target_set       : Target set for the computation (object of
                            Gridded Set)
        safe_set         : Safe set for the computation (object of
                            Gridded Set)
        dynamical_system : Dynamics of the system (object of
                            SystemDynamics)
        disturbance      : Disturbance perturbing the system (object of
                            Disturbance)
        time_horizon     : Time horizon for the computation (integer)

    Returns:
        value_function   : Value function at each instant of the dynamic
                            programming (A matrix of size
                            (T+1) x No_of_grid_points)
    """
    # List of value functions repeated across time instants
    value_function = [[0]*safe_set.state_space_grid.no_of_points] * (time_horizon+1)

    # Setting up the first value function --- Equation (7a)
    value_function[time_horizon] = target_set.indicator_function

    # Setting up the iteration --- Equation (7b). t\in\{T-1,\ldots,0\}
    for t in range(time_horizon-1,-1,-1):
        # Define V+ function
        print(t)
        value_function_plus = value_function[t+1]
        # if t<time_horizon-1:
            # #my_level_set_vplus = GriddedSet(safe_set.state_space_grid,\
            # #     lambda x: value_function_plus[safe_set.state_space_grid.get_index([[x[0]],[x[1]]])]>=0.99,'Vplus')
            # #my_level_set_vplus.display_the_set(True,'m')
            # x = safe_set.state_space_grid.x_vecs[0]
            # y = safe_set.state_space_grid.x_vecs[1]
            # X, Y = np.meshgrid(x, y)
            # zs = np.array(value_function_plus)
            # Z = zs.reshape(X.shape)

            # jet = plt.get_cmap('jet')

            # fig = plt.figure()
            # ax = fig.add_subplot(111, projection='3d')
            # ax.plot_surface(X, Y, Z, rstride = 1, cstride = 1, cmap = jet, linewidth = 0)
            # plt.xlabel('position')
            # plt.ylabel('velocity')
            # plt.title('Probability of task completion')
            # plt.show()


        # Iterate over grid_point_state and assign an index. Iterate only over the
        # safe set since the recursive definition has 1_K(x) outside.
        for current_state in safe_set.set_grid:
            # List of zeros for all each u
            expectation_value_function_plus_all_u = [0]*len(dynamical_system.input_space)
            # compute the index location
            current_state_array = np.array([[current_state[0]],[current_state[1]]])
            current_state_indx = safe_set.state_space_grid.get_index(current_state_array)
            for u,u_indx in zip(dynamical_system.input_space,range(len(dynamical_system.input_space))):
                # List of zeros for all each u
                expectation_value_function_plus_fixed_u = 0
                # Computation of expectation over d for a fixed u
                for d in disturbance.sample_space:
                    try:
                        xplus_index = dynamical_system.get_xplus(current_state_array,u,d)
                        expectation_value_function_plus_fixed_u += \
                                value_function_plus[xplus_index] * \
                                disturbance.probability_dictionary[d]
                        #print(round(expectation_value_function_plus_fixed_u,2),end=",")
                    except RuntimeError:
                        print(current_state)
                        #pass
                #### End of for d in disturbance.sample_space:
                # Assign the computed expectation for the fixed u to the list
                # for all u's
                expectation_value_function_plus_all_u[u_indx] = \
                        expectation_value_function_plus_fixed_u
            #### End of for u,u_indx in zip(u_vec,range(len(u_vec))):
            # Compute the value function --- Equation (7b)
            value_function[t][current_state_indx] = \
                    max(expectation_value_function_plus_all_u)
        #### End of for current_state in ....
    #### End of for t in ....
    return value_function
