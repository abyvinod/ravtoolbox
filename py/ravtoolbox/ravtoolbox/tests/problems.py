import numpy as np
from Grid import Grid 
from GriddedSet import GriddedSet
import problem_formulation

# Define target set
state_space=Grid(2, {'x1': {'min': -1, 'max': 1, 'step': 0.1}, 'x2': {'min': -1, 'max': 1, 'step': 0.1}})
# Define target set
target_set=GriddedSet(state_space,target_set_level_function)
# Define safe set
safe_set=GriddedSet(state_space,safe_set_level_function)

