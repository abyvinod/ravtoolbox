import ravtoolbox as rvt
import numpy as np

sample_space = np.array([1,2,3,4,5])
pmf = np.array([0.2,0.1,0.2,0.3,0.2])

dist = rvt.Disturbance(sample_space, pmf, 'discrete')
dist.histogram()
