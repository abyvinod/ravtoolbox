""" Testing python scripts

    Usage:
        cd into the ravtoolbox scripts
        >>> exec(open("./tests/testingAby.py").read())

"""
try: 
    exec(open("./clear_modules.py").read())
    print('Clearing the modules')
except:
    print('No need to clear the modules')

from Grid import Grid
from GriddedSet import GriddedSet
from Disturbance import Disturbance
from SystemDynamics import SystemDynamics

# Create a grid for the 2-D unit box centered at origin 
grid = Grid(2, {'x1': {'min': -0.5, 'max': 0.5, 'step': 0.01}, 'x2': {'min': -0.5, 'max': 0.5, 'step': 0.01}})
# Create a set for a circle centered at origin of radius 0.5
my_set = GriddedSet(grid,lambda x: x[0]**2+x[1]**2-0.25<=0)
#my_set.display_the_set()
sampling_time=0.1
A=[[1,sampling_time],[0,1]]
B=[[(sampling_time**2)/2],[sampling_time]]
F=B
system_dynamics=SystemDynamics(A,B,F,grid)
xplus_now=system_dynamics.get_xplus(50,1,1)
try:
    value=grid.get_state_from_index(xplus_now)
    print(grid.get_state_from_index(50))
    print(value)
except SyntaxError:
    print('Out of bounds')

""" Testing Disturbance Class"""
print('Testing Disturbance class')
sample_space=np.linspace(-1,1,9)
disturbance=Disturbance(sample_space,[1/len(sample_space) for i in range(9)],'discrete')
print(disturbance.probability_dictionary)
disturbance.histogram()
