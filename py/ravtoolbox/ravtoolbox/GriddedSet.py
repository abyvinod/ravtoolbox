import numpy as np
import matplotlib.pyplot as plt

class GriddedSet(object):
    """ Class to define sets in the state space.

        Variables:
            indicator_function          : Points in the grid of the state space
                                            classified as one if inside the set
                                            and zero if outside
            set_defining_level_function : The function whose zero sublevel set
                                            defines the set
            state_space_grid            : The grid based on which the state space was
                                            defined
            set_grid                    : Grid points of concern for this set
            set_grid_no_of_points       : No. of grid points of concern for this set
            name_of_the_set             : Name of the set

        Inputs for the constructor:
            state_space_grid            : The grid based on which the state space was
                                            defined
            set_defining_level_function : The function whose zero sublevel set
                                            defines the set

        Usage:
            >>> # Create a grid for the 2-D unit box centered at origin
            >>> grid = Grid(2, {'x1': {'min': -0.5, 'max': 0.5, 'step': 0.1}, 'x2': {'min': -0.5, 'max': 0.5, 'step': 0.1}})
            >>> # Create a set for a circle centered at origin of radius 0.5
            >>> my_set=GriddedSet(grid,lambda x: x[0]^2+x[1]^2-0.25<=0)

        TODO:
            * Nothing for now! Yay!
    """

    def __init__(self,grid_object,set_defining_level_function,name_of_the_set):
        """Constructor for the gridded set class."""
        # Storing the level function
        self.set_defining_level_function = set_defining_level_function
        # Initializing the indicator function as zeros(grid.no_of_points,1)
        self.indicator_function = [0] * grid_object.no_of_points
        # Storing the grid for the statespace
        self.state_space_grid = grid_object
        # Populating the indicator function list and gridded_set_grid
        self.set_grid=[]
        for gridpoint,index in zip(grid_object.mesh, range(grid_object.no_of_points)):
            if set_defining_level_function(gridpoint):
                self.indicator_function[index] = 1
                self.set_grid.append(gridpoint)
        # Name of the set
        self.name_of_the_set=name_of_the_set
        # No of points in the gridded_set_grid
        self.set_grid_no_of_points=sum(self.indicator_function)

    def display_the_set(self, show, color=0, ax=0):
        """ Display the set when state space is in R^2. The function will raise
        an AttributeError if called for higher dimension grid.

        Parameters:
            show    : Flag to show the plot or not
            color   : Color to plot
            ax      : Axis to plot

        Returns:
            CS      : Contour line information
        """
        if self.state_space_grid.dim==2:
            if show == True:
                fig=plt.figure(1)
                ax=fig.add_subplot(111)
            X, Y = np.meshgrid(self.state_space_grid.x_vecs[0],\
                    self.state_space_grid.x_vecs[1])
            CS = ax.contour(X, Y, \
                     np.reshape(self.indicator_function,\
                     (len(self.state_space_grid.x_vecs[0]),\
                      len(self.state_space_grid.x_vecs[1]))),\
                      levels=[0.999],colors=color,label=self.name_of_the_set)
            #plt.clabel(CS, fontsize=20)
            ax.set_aspect('equal')
            #plt.title(self.name_of_the_set)
            plt.grid()
            if show == True:
                plt.grid(True)
                plt.show(block=True)
        else:
            raise RuntimeError('This function is valid only for dimension of\
            the state space =2')
        return CS
