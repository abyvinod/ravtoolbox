import numpy as np
import Grid

class ReachAvoidSet(Grid.Grid):
    # constructor
    def __init__(self,set_type,dim,x_dict):
        self._type = set_type
        super(ReachAvoidSet, self).__init__(dim, x_dict)
