""" Testing the classes defined in this package

    Usage:
        cd into the ravtoolbox scripts
        >>> exec(open("./tests/testing.py").read())

"""
## Use autoreload in ipython for this
# try:
    # exec(open("./clear_modules.py").read())
    # print('Clearing the modules')
# except:
    # print('No need to clear the modules')

from Grid import Grid
from GriddedSet import GriddedSet
from Disturbance import Disturbance
from SystemDynamics import SystemDynamics
from random import uniform
import numpy as np
import matplotlib.pyplot as plt

""" Testing Grid Class"""
print('Testing Grid class => using a finer grid')
# Create a grid for the 2-D unit box centered at origin
grid = Grid(2, {'x1': {'min': -0.5, 'max': 0.5, 'step': 0.4},\
                'x2': {'min': -0.5, 'max': 0.5, 'step': 0.1}})
# Print out the mesh
grid.print_mesh()
print(grid.mesh, grid.no_of_points)

#########
# Must comment out runtime error restricting Grid class to only two dimensions
# for following code to work
# #
# grid = Grid(3, {'x1': {'min': 0, 'max': 1, 'step': 0.5},
#                 'x2': {'min': -0.5, 'max': 0.5, 'step': 0.5},
#                 'x3': {'min': -1, 'max': 1, 'step': 0.1}})
#                 # 'x4': {'min': -2, 'max': 2, 'step': 2}})
# print(grid.mesh)
#########

grid = Grid(2, {'x1': {'min': -0.5, 'max': 0.5, 'step': 0.005},\
                'x2': {'min': -0.5, 'max': 0.5, 'step': 0.005}})

some_index=[20,27]
# See if the next two lines give you the same output
print('Next two lines should match')
print(grid.get_state_from_index(some_index[0]))
print(grid.get_state_from_index(grid.get_index(grid.get_state_from_index(some_index[0]))))
# See if the next three lines give you the same output
print('Next three lines should match')
print(grid.get_state_from_index(some_index[1]))
some_subscript=grid.get_subscript(grid.get_state_from_index(some_index[1]))
print(np.array([[grid.x_vecs[0][some_subscript[0]]],[grid.x_vecs[1][some_subscript[1]]]]))
print(grid.get_state_from_subscript(some_subscript))

""" Testing GriddedSet Class"""
print('Testing GriddedSet class')
# Create a set for a circle centered at origin of radius 0.5
my_set = GriddedSet(grid,lambda x: x[0]**2+x[1]**2-0.25<=0,'myset')
fig=plt.figure(1)
ax=fig.add_subplot(111)
my_set.display_the_set(True,'k',ax)

""" Testing SystemDynamics Class"""
print('Testing SystemDynamics class')
# Double integrator dynamics x[t+1]=Ax[t]+Bu[t]+Fd[t]
sampling_time=0.1
A=[[1,sampling_time],[0,1]]
B=[[(sampling_time**2)/2],[sampling_time]]
F=B
input_space=np.linspace(-0.1,0.1,10)
system_dynamics=SystemDynamics(A,B,F,grid,input_space)
for i in range(2):
    print('Iteration : %d' % i)
    grid_index=round(grid.no_of_points * uniform(0,1))
    current_state_correct=grid.get_state_from_index(grid_index)
    current_state=np.array([[current_state_correct[0][0]],[current_state_correct[1][0]]])
    xplus_now=system_dynamics.get_xplus(current_state,1,1)
    try:
        value=grid.get_state_from_index(xplus_now)
        print(current_state)
        print(value)
    except RuntimeError:
        print(current_state)
        print('Out of bounds')

""" Testing Disturbance Class"""
print('Testing Disturbance class')
sample_space=np.linspace(-1,1,9)
disturbance=Disturbance(sample_space,[1/len(sample_space) for i in range(9)],'discrete')
print(disturbance.probability_dictionary)
disturbance.histogram()
