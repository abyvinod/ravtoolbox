from Grid import Grid
import numpy as np

# make a grid
# 3d
# grid = Grid(3, {'x1': {'min': -1, 'max': 1, 'step': 0.05},
#                 'x2': {'min': -1, 'max': 1, 'step': 0.05},
#                 'x3': {'min': -1, 'max': 1, 'step': 0.05}})

# 2d
grid = Grid(2, {'x1': {'min': -1, 'max': 1, 'step': 0.05},
                'x2': {'min': -1, 'max': 1, 'step': 0.05}})

# iterate through the mesh and see if everything matches
for mesh_point in grid.mesh:
    # convert mesh_point to state (basically transpose array)
    state = mesh_point.reshape(grid.dim,1)
    # print(grid.get_state_from_index(grid.get_index(state)))
    # break
    # print(state)
    if not np.allclose(state,grid.get_state_from_index(grid.get_index(state))):
        print('index mismatch for state {0}'.format(state))
