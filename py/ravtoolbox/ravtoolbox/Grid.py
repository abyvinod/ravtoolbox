import numpy as np
import warnings
import math as math

# global constant for zero tolerance threshold
ZERO_TOL = 1e-12

class Grid(object):
    """ Create grids

        Variables:
            dim         : Dimension of the grid
            x_dict      : Dictionary to store the limits and step size across each dimension
            no_of_points: No. of points in the mesh
            x_vecs      : The vectors storing the points along each dimension
            mesh        : The gridpoints stored as a matrix of the form no_of_points x dim

        Inputs for the constructor:
            dim         : Dimension of the grid
            x_dict      : Dictionary to store the limits and step size across each dimension

        Usage:
            >>> # Create a grid of the 2-D  unit box centered about origin at a grid
            >>> # step size of 0.1 in each direction
            >>> grid = Grid(2, {'x1': {'min': -0.5, 'max': 0.5, 'step': 0.1}, \
                                'x2': {'min': -0.5, 'max': 0.5, 'step': 0.1}})
            >>> # Print the grid
            >>> print(grid.mesh)

        TODO:
            * Extension to nD case
            * mesh_to_hypercube(): Returns a nD-grid coordinates based on the
            coordinates contained in vectors in each dimension
    """
    def __init__(self, dim, x_dict):
        """ constructor for the grid class"""
        # currently only support two dimensions
        # if dim > 2:
        #     raise RuntimeError('The toolbox does not currently support '
        #         'computation for sets greater than two dimensions.')

        if len(x_dict.keys()) != dim:
            raise RuntimeError('Improper amount of keys provided in x_dict.')

        # number of dimensions
        self.dim = dim

        # dictionary storing the limits and step size
        self.x_dict = x_dict

        # Create x_vecs and finally assign it
        self.x_vecs = []
        for nd in range(1, dim+1):
            # key string for dimension 1 .. dim + 1
            key_string = 'x' + str(nd)

            # check if (x1_max - x1_min) is wholely divisible by x_step
            quotient = (x_dict[key_string]['max'] - x_dict[key_string]['min']) \
                / x_dict[key_string]['step']
            if quotient - math.floor(quotient) > ZERO_TOL:
                print(((x_dict[key_string]['max'] - x_dict[key_string]['min']) // \
                x_dict[key_string]['step']))
                # new max for dimension
                new_x_max = x_dict[key_string]['min'] \
                    + ((x_dict[key_string]['max'] - x_dict[key_string]['min']) // \
                    x_dict[key_string]['step']) * x_dict[key_string]['step']
                # TODO: change the wording of this warning
                warnings.warn('Step size for {0} does not evenly span from [{1:.3f}, {2:.3f}].\n'
                    'Reseting max of {0} to {3:.3f}'.format(key_string,
                    x_dict[key_string]['min'], x_dict[key_string]['max'],
                    new_x_max))
                # reset dictionary max
                x_dict[key_string]['max'] = new_x_max

            # x_max = x_min + (length-1)*x_step
            # We are not adding the plus one since index_vec starts the count from zero
            # Updating the dictionary with the length
            self.x_dict[key_string]['length'] = (x_dict[key_string]['max'] - \
                    x_dict[key_string]['min']) / x_dict[key_string]['step'] + 1

            # new length should be an integer, if not FREAK OUT!
            if self.x_dict[key_string]['length'] \
                - math.floor(self.x_dict[key_string]['length']) > ZERO_TOL:
                # print((x_dict[key_string]['max'] - \
                #         x_dict[key_string]['min']) / x_dict[key_string]['step'] + 1)
                raise RuntimeError('Somehow the length of the dimension vector is still '
                    'not an integer...')

            # Updating the vector of points along each dimension
            self.x_vecs.append(np.linspace(
                x_dict[key_string]['min'],
                x_dict[key_string]['max'],
                self.x_dict[key_string]['length']))

        # create hypercube mesh from grid vectors and determine total number
        # of points
        self.mesh, self.no_of_points = self.mesh_to_hypercube();

    def mesh_to_hypercube(self):
        """ Function to convert object's grid vectors to a n-d hypercube
        """
        # setup x1 .. xn vectors
        # dim_product provides total number of points
        dim_product = 1
        in_string = ''
        for n in range(0,self.dim):
            exec('x{0:d} = self.x_vecs[{1:d}]'.format(n+1,n))
            in_string += 'x{0:d},'.format(n+1) if n < self.dim - 1 else 'x{0:d}'.format(n+1)
            dim_product *= len(self.x_vecs[n])

        # make meshgrid
        exec('meshgrid_array = np.meshgrid(' + in_string + ')')

        # NUMPY SMASH!!! (or reshapes all vectors to column vectors)
        for n in range(0,self.dim):
            # SMASH
            exec('x{0:d}r = meshgrid_array[{1:d}].reshape(dim_product,1)'.format(n+1,n))
            if n == 0:
                exec('mesh = x{0:d}r'.format(n+1))
            else:
                # concatenate reshaped vector onto the mesh
                exec('mesh = np.concatenate((mesh,x{0:d}r), axis=1)'.format(n+1))

        # return
        return (locals()['mesh'], locals()['dim_product'])

    def get_subscript(self,state):
        """ Returns the (i,j) of the given state in the list of mesh. Note that
        this function will raise a syntax error if the index is out of bounds.

            Parameters
                state : The state whose grid point is to be found (numpy array)
                            and has to be dim x 1 array --- row vector

            Returns
                index_vec : (i,j) of the given state in the list of mesh
                            (1 x dim tuple)

            TODO
                * Robust to handle even row vectors
        """

        # start of nd subscript finding
        subscript_vec = [0] * self.dim
        for i in range(0, self.dim):
            key_string = 'x{0:d}'.format(i+1)
            xi_min = self.x_dict[key_string]['min']
            xi_step = self.x_dict[key_string]['step']
            dxi = state[i] - xi_min
            subscript_vec[i] = np.round(dxi / xi_step)

        # x1_min = self.x_dict['x1']['min']
        # x2_min = self.x_dict['x2']['min']
        # x1_step = self.x_dict['x1']['step']
        # x2_step = self.x_dict['x2']['step']
        # delta_x = state - np.array([[x1_min],[x2_min]])
        # step_x = [[x1_step], [x2_step]]
        # # index_vec = (x-x_start)./x_step
        # # We are not adding the plus one since index_vec starts the count from zero
        # index_vec = np.round(np.divide(delta_x, step_x))
        # if index_vec[0] < 0 \
        #     or index_vec[1] < 0 \
        #     or index_vec[0] >= self.x_dict['x1']['length'] \
        #     or index_vec[1] >= self.x_dict['x2']['length']:
        #     raise RuntimeError('Index is out-of-bounds')
        return tuple(int(subscript_vec[i]) for i in range(0, self.dim))

    def is_state_in_grid(self,state):
        """ Determine if state is in grid.

            Right now it assumes the grid is a hypercube.

            Parameters
                state : The state which is to be determined in or outside the
                        grid

            Returns
                isin : Boolean value of whether the state is in the grid or not

            TODO
                * Expand for non-hypercube determination.

        """
        if len(state) > self.dim or len(state) < self.dim:
            RuntimeError('state does not have same dimensions as grid')

        # assume state is in, initially
        isin = True
        for n in range(0, self.dim):
            key_string = 'x{0:d}'.format(n+1)
            if state[n] > self.x_dict[key_string]['max'] \
                or state[n] < self.x_dict[key_string]['min']:
                # state not in dimension
                isin = False
                break

        return isin

    def get_index(self,state):
        """ Returns the position of the given state in the list of grid points

            Parameters
                state : The state whose grid point is to be found (numpy array)
                            and has to be dim x 1 array --- row vector

            Returns
                index : Position of the given state in the list of grid points
                            (integer)

            TODO
                * Robust to handle even row vectors
        """
        if self.is_state_in_grid(state):
            index_vec = self.get_subscript(state)
            if self.dim == 2:
                index = int(index_vec[0] + self.x_dict['x1']['length'] * index_vec[1])
            elif self.dim == 3:
                index = int(index_vec[2]
                    + self.x_dict['x3']['length'] * index_vec[0]
                    + self.x_dict['x3']['length'] * self.x_dict['x1']['length'] * index_vec[1])
            else:
                RuntimeError('Code currently cannot handle finding index of system higher than 3 dimensions')
        else:
            index = -1

        return index

        # try:
        #     index_vec = self.get_subscript(state)
        #     # Order of indices is so because of the order of the for loops
        #     index = int(index_vec[0] + self.x_dict['x1']['length'] * index_vec[1])
        # except RuntimeError:
        #     index = -1
        # return index

    def get_state_from_index(self,index):
        """ Returns the state corresponding to the given index in the list of grid points

            Parameters
                index : Given position in the list of grid points (integer)

            Returns
                state : The state corresponding to the index (numpy array)
        """
        if index < 0 or index >= self.no_of_points:
            raise RuntimeError('Index is out-of-bounds')
        # return [[self.mesh[index][0]],[self.mesh[index][1]]]
        # line below should return n-dim state
        # also will probably want to keep states as numpy arrays
        return np.array([[self.mesh[index][i]] for i in range(0,self.dim)])

    def get_state_from_subscript(self,subscript):
        """ Returns the state corresponding to the given tuple (i,j)

            Parameters
                subscript : Given (i,j) (dim x 1 tuple)

            Returns
                state : The state corresponding to the index (numpy array)
        """
        if subscript[0] < 0 \
            or subscript[1] < 0 \
            or subscript[0] >= self.x_dict['x1']['length'] \
            or subscript[1] >= self.x_dict['x2']['length']:
            raise RuntimeError('Index is out-of-bounds')
        return [[self.x_vecs[0][subscript[0]]],[self.x_vecs[1][subscript[1]]]]

    def print_mesh(self):
        """ Print the mesh

            Parameters : None
            Returns : None
        """
        print(*[str(indx)+" => ("+str(grid_point[0])+','+str(grid_point[1])+')' for grid_point,indx in zip(self.mesh,range(self.no_of_points))],sep='\n')
        # slightly more consice print statement with more typical string format
        # style
        #print(*['{0:d} => ({0:f},{0:f})'.format(indx, grid_point)
        #    for grid_point,indx in zip(self.mesh, range(self.no_of_points))], sep='\n')
