""" Testing the reach_avoid_terminal.py

    Usage:
        Python --- cd into the ravtoolbox scripts
        >>> exec(open("./tests/testing.py").read())
        iPython --- cd into the ravtoolbox scripts
        In[.]: %run -t testing.py

    TODO:
        * Time_horizon, sampling_time, u_max, d_max together distorts the figure
          obtained. Numerical artifact or some theoretical problem?
          Viability fails if u_max=0.1
"""
from reach_avoid_terminal import reach_avoid_terminal
from Grid import Grid
from GriddedSet import GriddedSet
from SystemDynamics import SystemDynamics
from Disturbance import Disturbance
import matplotlib.pyplot as plt
import matplotlib as mplt
import numpy as np

mplt.rcParams.update({'font.size': 22})


""" Reach - avoid """
# grid = Grid(2, {'x1': {'min': -1, 'max': 1, 'step': 0.005}, 'x2': {'min': -1, 'max': 1, 'step': 0.005}})
# target_set = GriddedSet(grid,lambda x: abs(x[0])<=0.25 and abs(x[1])<=0.25,'Target set')
# safe_set = GriddedSet(grid,lambda x: abs(x[0])<=0.75 and abs(x[1])<=0.75,'Safe set')
# #safe_set = GriddedSet(grid,lambda x: True,'Safe set')
# levels=[0.1,0.3]
# input_space=np.linspace(-0.2,0.2,3)
# time_horizon=20
# print('This takes about 10 minutes')
########
# target_set = GriddedSet(grid,lambda x: x[0]>=0 and x[0]<=0.15 and x[1]>=0 and x[1]<=0.15,'Target set')
# safe_set = GriddedSet(grid,lambda x: abs(x[0])<=0.25 and abs(x[1])<=0.25,'Safe set')
# levels=[0.1,0.3]
""" Reach """
# grid = Grid(2, {'x1': {'min': -1, 'max': 1, 'step': 0.005}, 'x2': {'min': -1, 'max': 1, 'step': 0.005}})
# target_set = GriddedSet(grid,lambda x: x[0]**2+x[1]**2-0.0625<=0,'Target set')
# safe_set = GriddedSet(grid,lambda x: True,'Safe set')
# levels=[0.1,0.6]
# time_horizon=10
# input_space=np.linspace(-0.5,0.5,3)
# # input_space=np.linspace(-0.1,0.1,3)
# print('Reachability computation takes about 10 minutes')
""" Viability """
grid = Grid(2, {'x1': {'min': -1, 'max': 1, 'step': 0.005}, 'x2': {'min': -1, 'max': 1, 'step': 0.005}})
target_set = GriddedSet(grid,lambda x: True,'Target set')
safe_set = GriddedSet(grid,lambda x: abs(x[0])<=0.75 and abs(x[1])<=0.75,'Safe set')
levels = [0.1,0.6]
input_space = np.linspace(-0.5,0.5,3)
time_horizon = 10
print('Viability computation takes about 6 minutes')

sampling_time = 0.05
A = [[1,sampling_time],[0,1]]
B = [[(sampling_time**2)/2],[sampling_time]]
F = B
system_dynamics = SystemDynamics(A,B,F,grid,input_space)
sample_space = np.linspace(-0.5,0.5,3)
disturbance = Disturbance(sample_space,[0.3,0.4,0.3],'discrete')
#disturbance=Disturbance(sample_space,[0,1,0],'discrete')
#sample_space=0
#disturbance=Disturbance([0],[1],'discrete')
value_function = reach_avoid_terminal(target_set,safe_set,system_dynamics,
    disturbance,time_horizon)

"""Plotting the contour plot"""
fig = plt.figure(1)
ax = fig.add_subplot(111)
CS1 = target_set.display_the_set(False,'r',ax)
CS2 = safe_set.display_the_set(False,'k',ax)
my_level_set_lvl0 = GriddedSet(grid,\
     lambda x: value_function[0][grid.get_index([[x[0]],[x[1]]])]>=levels[0],\
     str(levels[0])+'-level_set')
CS3 = my_level_set_lvl0.display_the_set(False,'b',ax)
my_level_set_lvl1 = GriddedSet(grid,\
     lambda x: value_function[0][grid.get_index([[x[0]],[x[1]]])]>=levels[1],\
     str(levels[1])+'-level_set')
CS4 = my_level_set_lvl1.display_the_set(False,'m',ax)

lines = [ CS1.collections[0], CS2.collections[0], CS3.collections[0], CS4.collections[0]]
labels = ['target_set','safe_set',str(levels[0])+'-level_set',str(levels[1])+'-level_set']
plt.legend(lines, labels,loc='center left',fancybox=True, bbox_to_anchor=(1, 0.5))
#plt.xlim([-0.5,0.5])
#plt.ylim([-0.5,0.5])
plt.grid(True)
plt.xlabel('Position')
plt.ylabel('Velocity')
plt.tight_layout()
plt.show()

#"""Plotting the 3-D plot"""
x = grid.x_vecs[0]
y = grid.x_vecs[1]
X, Y = np.meshgrid(x, y)
zs = np.array(value_function[0])
Z = zs.reshape(X.shape)

jet = plt.get_cmap('jet')

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(X, Y, Z, rstride = 1, cstride = 1, cmap = jet, linewidth = 0)
plt.xlabel('Position')
plt.ylabel('Velocity')
plt.tight_layout()
# plt.show()
