import numpy as np
import matplotlib.pyplot as plt

class Disturbance(object):
    """ Disturbance specification.

        Variables:
            _type                       : Flag for discrete/continuous (private)
            sample_space                : Values disturbance can take
            probability_mass_function   : Probability mass function for each value
                                            in the sample_space
            probability_dictionary      : Dictionary storing omega to probability

        Inputs for the constructor:
            _type                       : Flag for discrete/continuous (private)
            sample_space                : Values disturbance can take
            probability_mass_function   : Probability mass function for each value
                                            in the sample_space

        Usage:
            >>> # Create a disturbance random variable with sample space ={-1,0,1} and
            >>> # probability mass function of {0.4,0.2,0.4}
            >>> dist = Disturbance(np.linspace(-1,1,3),np.array([0.4,0.2,0.4]))
            >>> # Print the disturbance type
            >>> print(dist._type)

        TODO:
            * histogram() function
            * Extension to disturbance as a continuous random variable
    """
    def __init__(self, sample_space, probability_mass_function,_type):
        """ constructor for the disturbance class"""
        self._type = _type
        self.probability_dictionary = {}
        self.sample_space = sample_space
        self.probability_mass_function = probability_mass_function
        for omega,prob in zip(self.sample_space,probability_mass_function):
            self.probability_dictionary[omega]=prob


    def histogram(self):
        """ method to plot the probability mass function of the disturbance
            object.

            TODO: The stem plot cuts off the minimum and maximum of the pmf,
                  may need some tweaking for better visualization
        """
        plt.figure()
        plt.stem(self.sample_space, self.probability_mass_function)
        plt.show()
