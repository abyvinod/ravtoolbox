import numpy as np

class SystemDynamics(object):
    """ System dynamics
        x[k+1] = A x[k] + B u[k] + F d[k]

        Variables:
            A                : System matrix
            B                : Input matrix
            F                : Disturbance matrix
            state_space_grid : State space grid

        Inputs for the constructor:
            A                : System matrix
            B                : Input matrix
            F                : Disturbance matrix
            state_space_grid : State space grid
            input_space      : input vector

        Usage:
             Commented code for basic usage

        TODO:
            * get
    """
    def __init__(self, A, B, F, state_space_grid,input_space):
        """ Constructor for SystemDynamics"""
        self.A = A
        self.B = B
        self.F = F
        self.state_space_grid = state_space_grid
        self.input_space = input_space

    def get_xplus(self,current_state,u,d):
        """ Find the index of the grid point of x[k+1] given x[k],u[k] and d[k]

            Parameters:
                current_state   : Given current state as a np.array (nx1)
                u               : Input
                d               : Disturbance

            Returns:
                grid_point_plus : Index of the state at the next instant
        """
        #current_state = np.array([[current_state_list[i]] for i in range(0,len(current_state))])
        xplus = np.dot(self.A , current_state) + np.dot(self.B,u) + np.dot(self.F,d)
        return self.state_space_grid.get_index(xplus)
